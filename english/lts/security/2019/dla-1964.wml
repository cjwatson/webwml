<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In sudo, a program that provides limited super user privileges to
specific users, an attacker with access to a Runas ALL sudoer account
can bypass certain policy blacklists and session PAM modules, and can
cause incorrect logging, by invoking sudo with a crafted user ID. For
example, this allows bypass of (ALL,!root) configuration for a
"sudo -u#-1" command.</p>

<p>See <a href="https://www.sudo.ws/alerts/minus_1_uid.html">https://www.sudo.ws/alerts/minus_1_uid.html</a> for further
information.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.10p3-1+deb8u6.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1964.data"
# $Id: $
