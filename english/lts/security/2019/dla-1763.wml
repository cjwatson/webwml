<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities were found in the PuTTY SSH client, which could
result in denial of service and potentially the execution of arbitrary
code. In addition, in some situations random numbers could potentially be
re-used.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.63-10+deb8u2.</p>

<p>We recommend that you upgrade your putty packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1763.data"
# $Id: $
