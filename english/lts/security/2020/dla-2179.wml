<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Following CVEs were reported against the jackson-databind source package:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10968">CVE-2020-10968</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.aoju.bus.proxy.provider.remoting.RmiProvider (aka bus-proxy).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10969">CVE-2020-10969</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    javax.swing.JEditorPane.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11111">CVE-2020-11111</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.activemq.* (aka activemq-jms, activemq-core,
    activemq-pool, and activemq-pool-jms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11112">CVE-2020-11112</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.commons.proxy.provider.remoting.RmiProvider
    (aka apache/commons-proxy).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11113">CVE-2020-11113</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.openjpa.ee.WASRegistryManagedRuntime (aka openjpa).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11619">CVE-2020-11619</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.springframework.aop.config.MethodLocatingFactoryBean
    (aka spring-aop).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11620">CVE-2020-11620</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.commons.jelly.impl.Embedded (aka commons-jelly).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u14.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2179.data"
# $Id: $
