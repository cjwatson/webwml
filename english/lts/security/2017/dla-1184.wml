<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An integer overflow vulnerability was found in optipng, an advanced
PNG optimizer that also recognizes other external file formats. This
may lead to arbitrary code execution when a maliciously crafted TIFF
file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.6.4-1+deb7u3.</p>

<p>We recommend that you upgrade your optipng packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1184.data"
# $Id: $
