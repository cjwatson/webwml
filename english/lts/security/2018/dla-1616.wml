<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were discovered in libextractor, a library for
extracting meta data from files of arbitrary type. An out-of-bounds
read in common/convert.c and a NULL Pointer Dereference in the OLE2
extractor may lead to a denial-of-service (application crash).</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.3-2+deb8u4.</p>

<p>We recommend that you upgrade your libextractor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1616.data"
# $Id: $
