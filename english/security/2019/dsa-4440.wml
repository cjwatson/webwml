<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the BIND DNS server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5743">CVE-2018-5743</a>

    <p>Connection limits were incorrectly enforced.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5745">CVE-2018-5745</a>

    <p>The "managed-keys" feature was susceptible to denial of service by
    triggering an assert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6465">CVE-2019-6465</a>

    <p>ACLs for zone transfers were incorrectly enforced for dynamically
    loadable zones (DLZs).</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 1:9.10.3.dfsg.P4-12.3+deb9u5.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4440.data"
# $Id: $
