<define-tag pagetitle>Debian 9 aktualisiert: 9.11 veröffentlicht</define-tag>
<define-tag release_date>2019-09-08</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="0ab7616e721c26339dd0738e8d984bb6f4e25b8c" maintainer="Erik Pfannenstein"


<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Das Debian-Projekt freut sich, die elfte Aktualisierung seiner 
Oldstable-Distribution Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen.</p>

<p>Diese Zwischenveröffentlichung ist vor allem eine Ergänzung zur kürzlich 
veröffentlichten Revision 9.10, um ein kritisches Problem beim Installer zu 
lösen, welches beim Testen der Abbilder entdeckt wurde.</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine völlig neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da 
deren Pakete nach der Installation mit Hilfe eines aktuellen 
Debian-Spiegelservers auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction bogl "iswspace anstelle von isspace aufrufen, behebt Absturz bei U+FEFF">
<correction debian-installer "Neubau gegen proposed-updates">
<correction debian-installer-netboot-images "Neubau gegen proposed-updates">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Oldstable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständigen Listen von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt;, oder kontaktieren das Stable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>


