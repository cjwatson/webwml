#use wml::debian::cdimage title="Как организовать зеркало компакт-дисков Debian" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="be6f9cbfcdcebe5026a31b4f3171a70f1a2bd9c1" maintainer="Lev Lamberov"

<p>Чтобы держать зеркало образов CD Debian, вам нужна машина под
управлением Linux или другого клона Unix с постоянным надёжным
подключением к Интернет. Зеркала образов CD Debian хранят образы
CD и DVD <tt>.iso</tt> различного размера, файлы <a
href="https://www.einval.com/~steve/software/jigdo/">jigdo</a> (<tt>.jigdo</tt> и
<tt>.template</tt>), файлы <a
href="https://ru.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
(<tt>.torrent</tt>) и проверочные файлы для этих образов
(<tt>SHA512SUMS*</tt> и <tt>SHA256SUMS*</tt>).</p>

<toc-display/>

#______________________________________________________________________

<toc-add-entry name="master">Главный сайт</toc-add-entry>

<p><!-- Зеркалируемые образы берутся с двух сайтов&nbsp;&mdash; образы
стабильного дистрибутива с одного, а бета/нестабильного/тестируемого&nbsp;&mdash;
с другого. -->URL главного сайта приведён ниже. Однако, <strong>пожалуйста</strong>
берите образы с другого близлежащего зеркала (списки зеркал: <a
href="../http-ftp/">HTTP/FTP</a>, <a href="rsync-mirrors">rsync</a>),
если это возможно. Доступ к главному сайту сразу после выхода нового
выпуска может быть ограничен.</p>

<p>Учитывайте также <strong>огромный</strong> объём данных, хранящихся
в этих каталогах&nbsp;&mdash; см. подробную информацию о том, как уменьшить
размер путём отказа от части файлов <a href="#exclude">ниже</a>.</p>

<ul>

  <li>Образы стабильного дистрибутива (при выходе нового выпуска обновляются):<br>

    <a href="https://cdimage.debian.org/debian-cd/"
    ><tt>https://cdimage.debian.org/debian-cd/</tt></a><br>

    <a href="ftp://cdimage.debian.org/debian-cd/"
    ><tt>ftp://cdimage.debian.org/debian-cd/</tt></a><br>

    <tt>rsync://cdimage.debian.org/debian-cd/</tt>

  </li>

  <li>Еженедельные образы:<br>

    <a href="https://cdimage.debian.org/cdimage/weekly-builds/"
    ><tt>https://cdimage.debian.org/cdimage/weekly-builds/</tt></a><br>

    <a href="ftp://cdimage.debian.org/cdimage/weekly-builds/"
    ><tt>ftp://cdimage.debian.org/cdimage/weekly-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/weekly-builds/</tt>

  </li>

  <li>Ежедневные образы:<br>

    <a href="https://cdimage.debian.org/cdimage/daily-builds/"
    ><tt>https://cdimage.debian.org/cdimage/daily-builds/</tt></a><br>

    <a href="ftp://cdimage.debian.org/cdimage/daily-builds/"
    ><tt>ftp://cdimage.debian.org/cdimage/daily-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/daily-builds/</tt>

  </li>

</ul>
#______________________________________________________________________

<toc-add-entry name="httpftp">Зеркалирование через HTTP/FTP не
рекомендуется</toc-add-entry>

<p>Для обновления зеркала не следует использовать FTP или HTTP.
Эти методы передачи данных сопряжены с высокой вероятностью ошибок
из-за чрезвычайно большого размера файлов.</p>

<p>Более того, HTTP и FTP не производят проверку целостности
загруженных данных, весьма вероятно, что обрыв связи при загрузке
или повреждение данных при передаче вообще не будет замечено.</p>
#______________________________________________________________________

<toc-add-entry name="rsync">Рекомендуется зеркалирование с помощью
rsync</toc-add-entry>

<p>Программа <a
href="http://rsync.samba.org/"><kbd>rsync</kbd></a>&nbsp;&mdash;
удачное решение этих проблем.  Она менее эффективна, чем другие способы
зеркалирования, специфичные для Debian, но её проще настроить.  Более
того, она обеспечивает то, что все файлы будут переданы корректно, и
что метаданные (например, отметки времени) будут сохранены так же, как
и данные файла.</p>

<p>См. примеры <kbd>--include</kbd> и <kbd>--exclude</kbd> в разделе <a
href="#exclude">Отказ от зеркалирования отдельных файлов</a>. На
отдельной странице доступен <a href="rsync-mirrors">список зеркал
rsync</a>.</p>

<p>Используйте параметры <strong><kbd>--times --links --hard-links --partial
--block-size=8192</kbd></strong>. При этом будут сохранены время
последнего изменения, символические и жёсткие связи, и будут
использоваться блоки размером 8192 байта (наиболее подходящие для
образов CD). Если время последнего изменения и размер те же самые,
<kbd>rsync</kbd> не будет трогать файл вообще, так что
<kbd>--times</kbd> действительно надо использовать всегда.</p>
#______________________________________________________________________

<toc-add-entry name="jigdolite">Зеркалирование с помощью jigdo-lite не
рекомендуется</toc-add-entry>

<p>Последние версии программы <a
href="https://www.einval.com/~steve/software/jigdo/"><kbd>jigdo-lite</kbd></a> поддерживают
пакетную загрузку нескольких образов. Однако, мы не рекомендуем
использовать для создания зеркал дисков Debian
<kbd>jigdo-lite</kbd>&nbsp;&mdash; лучше использовать
<kbd>jigdo-mirror</kbd>.</p>
#______________________________________________________________________

<toc-add-entry name="jigdomirror">Рекомендуется зеркалирование с помощью
jigdo-mirror</toc-add-entry>

<p>На самом деле, это означает: зеркалировать файлы <tt>.iso</tt> с
помощью <a
href="https://www.einval.com/~steve/software/jigdo/"><kbd>jigdo-mirror</kbd></a>, а затем
(если вы хотите зеркалировать также другие файлы, например, <tt>.jigdo</tt>
и <tt>.template</tt>) запустить rsync для синхронизации каталога. При
этом будут загружены остальные файлы. В настройке могут помочь скрипты
на <a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/">этой
странице</a>.</p>

<p>Многие люди сопровождают "обычные" зеркала Debian
(<kbd>debian/</kbd>), или просто имеют хорошую связь с таким зеркалом.
Это означает, что у них уже есть файлы <tt>.deb</tt>, содержащиеся на
образах компакт-дисков и DVD. Очевидный вопрос:
почему бы не использовать те же самые файлы на образах компакт-дисков и DVD?</p>

<p><kbd>jigdo-mirror</kbd>&nbsp;&mdash; это программа, позволяющая
генерировать наборы образов компакт-дисков и DVD Debian, используя файлы "нормального"
зеркала и несколько дополнительных файлов шаблонов jigdo.</p>

<p>Для начала, вам понадобятся файлы шаблонов jigdo. См. ссылки на <a
href="../jigdo-cd">странице информации jigdo</a>. Нужно загрузить
файлы для каждой архитектуры, для которой вы хотите собрать образы.</p>

<p>Создайте файл <kbd>~/.jigdo-mirror</kbd> для конфигурирования программы.
Вот пример:</p>

<pre>
jigdoDir="/where/you/keep/mirrors/debian-cd/current/jigdo"
imageDir="/where/you/keep/mirrors/debian-cd/current/images"
tmpDir="/where/you/keep/mirrors/debian-cd/current/images"
debianMirror="file:/where/you/keep/mirrors/debian"
include='i386/|sparc/|powerpc/|source/'; exclude='-1\.'
</pre>

<p>Переменные <i>include</i> и <i>exclude</i> содержат список
архитектур, для которых вы хотите создать образы (регулярные выражения).
Более подробную информацию см. на странице руководства <kbd>jigdo-mirror</kbd>
или в исходном коде (это скрипт оболочки с обширными комментариями).</p>

<p>После конфигурирования просто запустите <kbd>jigdo-mirror</kbd> и
она сделает всё самостоятельно. Программа выводит на экран большое
количество информации и, вероятно, её работа займёт некоторое время.
Поэтому мы предлагаем вам принять меры (запустить с экрана, но
перенаправить вывод в файл и т.д.).</p>
#______________________________________________________________________

<toc-add-entry name="pushmirror">Как организовать автоматическое
зеркало</toc-add-entry>

<p>Как только становятся доступны новые образы, главный сайт может
отправить на зеркала сообщение, чтобы они сразу начинали обновление.
Таким образом новые данные "вталкиваются" на зеркала вместо того,
чтобы зеркала их "вытягивали" при следующем ежедневном обновлении.
Это позволяет быстрее распространять образы новых выпусков.</p>

<p>Если вы хотите, чтобы ваше зеркало входило в систему обновления,
посетите <a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/"
>эту страницу</a>.</p>
#______________________________________________________________________

<toc-add-entry name="exclude">Отказ от зеркалирования отдельных
файлов</toc-add-entry>

<p>Чтобы уменьшить размер дискового пространства, необходимого для
хранения зеркала дисков Debian, можно отказаться от зеркалирования
некоторых файлов. Следующие инструкции описывают ключи командной
строки для <kbd>rsync</kbd>, но могут помочь даже если вы используете
другие инструменты. В случае <kbd>rsync</kbd> ключи
<kbd>--include</kbd> и <kbd>--exclude</kbd> рассматриваются в порядке
следования в командной строке, и то, загружается файл или нет,
определяется первым ключом, шаблон которого соответствует файлу.</p>

<ul>

  <li><strong>Не загружать исходный код:</strong>
  <kbd>--exclude=source/</kbd><br>

  Не будут зеркалироваться образы, содержащие исходный код. Имейте в
  виду, что некоторые полагают несоответствующим условиям лицензии
  GPL нахождение на сервере скомпилированных программ, распространяемых
  на условиях этой лицензии, если их исходный код <em>на том же
  сервере</em> отсутствует.</li>

  <li><strong>Не загружать полные образы:</strong>
  <kbd>--include='*netinst*.iso'
  --exclude='*.iso'</kbd><br>

  Не загружать образы CD/DVD для всех архитектур, <em>за
  исключением</em> образов для
  сетевой установки. Мы рекомендуем всегда зеркалировать эти
  небольшие образы&nbsp;&mdash; по соотношению пользы от них к их
  размеру они чрезвычайно полезны!</li>

  <li><strong>Не загружать полные образы для всех архитектур, кроме
  i386:</strong>
  <kbd>--include='*netinst*.iso'
  --include='i386/**.iso' --exclude='*.iso'</kbd><br>

  Как в прошлом примере, но с загрузкой всех образов CD/DVD для
  архитектуры i386.</li>

  <li><strong>Не загружать полные образы, за исключением первых трёх
  дисков для архитектуры i386:</strong>
  <kbd>--include='*netinst*.iso' --include='i386/**-[1-3].iso'
  --exclude='*.iso'</kbd><br>

  Полный набор образов для архитектуры i386 может всё ещё быть слишком
  большим для вас, включая образы DVD и двухслойных DVD.  Эта команда
  не будет загружать образы <tt>.iso</tt>, за исключением образов для
  сетевой установки и первых трёх DVD для архитектуры
  i386.</li>

  <li><strong>Не загружать образы для нескольких архитектур, кроме i386:</strong>
  <kbd>--exclude=alpha/ --exclude=arm/ --exclude=hppa/ --exclude=hurd/
  --exclude=ia64/ --exclude=m68k/ --exclude=mips/ --exclude=mipsel/
  --exclude=powerpc/ --exclude=s390/ --exclude=sh/
  --exclude=sparc/</kbd><br>

  Загрузить только полный набор файлов для архитектуры i386, не
  включая файлы <tt>.jigdo</tt>, <tt>.iso</tt> и т.д. для других
  архитектур.<br>

  <strong>Проверьте список архитектур до начала зеркалирования, список
  меняется, и данные примеры могут устареть!</strong></li>

</ul>
#______________________________________________________________________

<toc-add-entry name="names">Как называются образы <tt>.iso</tt> и каков
их размер</toc-add-entry>

<p>Различные образы <tt>.iso</tt> различаются по именам, что позволяет
загрузить только нужные файлы:</p>

<ul>

  <li><strong><tt>*-netinst.iso</tt></strong>: Один образ для каждой
  архитектуры, до 500&nbsp;МБ</li>

  <li><strong><tt>*-dvd.iso</tt></strong> (однослойный DVD):
  Несколько образов, каждый размером до до 4482&nbsp;МБ. Для buster имеются
  до <strong>16</strong> образов DVD для каждой архитектуры. Серверы Debian
  предоставляют лишь небольшую часть образов DVD в виде файлов .iso для
  прямой загрузки: 3 для amd64, 3 для i386 и 1 для каждой оставшейся
  архитектуры. Остальные образы предоставляются только с помощью
  jigdo.</li>

  <li><strong><tt>*-bd.iso</tt></strong> (однослойный Blu-Ray): Как
  и в случае с DVD, но отдельные образы имеют размер до 23&nbsp;ГБ.
  Эти образы доступны только в виде jigdo-файлов для ограниченного
  набора архитектур (amd64 и i386) и для образов с исходным кодом.</li>

  <li><strong><tt>*-dlbd.iso</tt></strong> (двухслойный Blu-Ray): Как
  и в случае с DVD, но отдельные образы имеют размер до 48&nbsp;ГБ.
  Эти образы доступны только в виде jigdo-файлов для ограниченного
  набора архитектур (amd64 и i386) и для образов с исходным кодом.</li>

  <li><strong><tt>*-STICK16GB*.iso</tt></strong> (16ГБ образы для USB): Как
  и в случае с DVD, но отдельные образы имеют размер до 16&nbsp;ГБ.
  Эти образы доступны только в виде jigdo-файлов для ограниченного
  набора архитектур (amd64 и i386) и для образов с исходным кодом.</li>

</ul>
#______________________________________________________________________

<toc-add-entry name="register">Регистрация зеркала</toc-add-entry>

<p>Чтобы сделать ваше зеркало полезным широкой аудитории, вы можете
зарегистрировать его в нашем списке зеркал,
<a href="../http-ftp/">этом</a> или <a href="rsync-mirrors">этом</a>.
Тем не менее, поскольку полные образы очень велики, это может привести
к трафику в несколько гигабайтов в день.</p>

<p>Вы можете зарегистрировать ваше зеркало либо заполнив
<a href="$(HOME)/mirror/submit">форму информации о зеркале</a>
(имейте в виду, что поля CDImage-* очень важны), либо отправив
сообщение по адресу
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;debian-cd&#64;lists.debian.org">\
debian-cd&#64;lists.debian.org</a>.</p>

<p>Мы будем рады появлению любых новых зеркал образов CD. Заранее благодарим вас!</p>
