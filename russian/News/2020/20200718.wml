#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 9: выпуск 9.13</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о тринадцатом (и последнем) обновлении своего
предыдущего стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>После выпуска данной редакции команды безопасности и выпусков Debian более не
будут работать над обновлениями Debian 9. Пользователи, желающие продолжать
получать поддержку безопасности, должны выполнить обновление до Debian 10, либо
обратиться к странице <url "https://wiki.debian.org/LTS">, на которой приводится
подмножество архитектур и пакетов, которые будут поддерживаться в рамках проекта долгосрочной поддержки.
</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное обновление предыдущего стабильного выпуска вносит несколько важных исправлений для следующих
пакетов:</p>
<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction acmetool "Повторная сборка с использованием свежей версии golang для добавления исправлений безопасности">
<correction atril "dvi: снижение риска от введения команд путём обрамления в кавычки имён файлов [CVE-2017-1000159]; исправление проверки переполнения в движке tiff [CVE-2019-1010006]; tiff: обработка ошибки от TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula "Добавление переходного пакета bacula-director-common, чтобы избежать потери /etc/bacula/bacula-dir.conf при очистке; установка суперпользователя в качестве владельца PID-файлов">
<correction base-files "Обновление /etc/debian_version для данной редакции">
<correction batik "Исправление подделки запроса на стороне сервера через атрибуты xlink:href [CVE-2019-17566]">
<correction c-icap-modules "Поддержка ClamAV 0.102">
<correction ca-certificates "Обновление набора Mozilla CA до версии 2.40, запрет недоверенных корневых сертификатов Symantec и устаревшего сертификата <q>AddTrust External Root</q>; удаление почтовых сертификатов">
<correction chasquid "Повторная сборка с использованием свежей версии golang для добавления исправлений безопасности">
<correction checkstyle "Исправление введения внешней XML-сущности [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "Новый выпуск основной ветки разработки [CVE-2020-3123]; обновления безопасности [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "Новая версия основной ветки разработки, совместимая с новыми версиями Thunderbird">
<correction cram "Игнорирование ошибок тестирования для исправления проблем сборки">
<correction csync2 "Отмена команды HELLO, если требуется SSL">
<correction cups "Исправление переполнения динамической памяти [CVE-2020-3898] и ошибки <q>функция `ippReadIO` может производить чтение до начала пола расширения</q> [CVE-2019-8842]">
<correction dbus "Новый стабильный выпуск основной ветки разработки; предотвращение отказа в обслуживании [CVE-2020-12049]; предотвращение использования указателей после освобождения памяти, если у двух имён пользователей имеется один uid">
<correction debian-installer "Обновление для поддержки ABI ядра Linux версии 4.9.0-13">
<correction debian-installer-netboot-images "Повторная сборка с учётом stretch-proposed-updates">
<correction debian-security-support "Обновление статуса поддержки некоторых пакетов">
<correction erlang "Исправление использования слабых шифров TLS [CVE-2020-12872]">
<correction exiv2 "Исправление отказа в обслуживании [CVE-2018-16336]; исправление слишком чрезмерно ограничительного исправления для CVE-2018-10958 и CVE-2018-10999">
<correction fex "Обновление безопасности">
<correction file-roller "Исправление безопасности [CVE-2020-11736]">
<correction fwupd "Новый выпуск основной ветки разработки; использование CNAME для перенаправления на корректный CDN для получения метаданных; не прерывать загрузку, если XML-файл с метаданными неверен; добавление открытых GPG-ключей Linux Foundation для микропрограмм и метаданных; увеличение ограничения размера метаданных до 10МБ">
<correction glib-networking "Возвращение ошибки плохой идентификации, если идентификация не задана [CVE-2020-13645]">
<correction gnutls28 "Исправление повреждения содержимого памяти [CVE-2019-3829]; исправление утечки памяти; добавление поддержки для тикетов сессии с нулевой длиной, исправление ошибок соединения для сессий TLS1.2 на некоторых хостингах">
<correction gosa "Ужесточение проверки успешности/неудачи LDAP [CVE-2019-11187]; исправление совместимости с более новыми версиями PHP; обратный перенос нескольких заплат; замена (де-) сериализации на функции json_encode/json_decode для снижения риска введения PHP-объектов [CVE-2019-14466]">
<correction heartbleeder "Повторная сборка с использованием свежей версии golang для добавления исправлений безопасности">
<correction intel-microcode "Откат некоторых микрокодов до предыдущих редакций, обход зависаний при загрузке на Skylake-U/Y и Skylake Xeon E3">
<correction iptables-persistent "Не переходить в состояние ошибки, если произошла ошибка modprobe">
<correction jackson-databind "Исправление многочисленных проблем безопасности, касающихся BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 и CVE-2019-17267]">
<correction libbusiness-hours-perl "Явное обозначение нумерации года с помощью 4 цифр, исправляющее сборку и использование">
<correction libclamunrar "Новый стабильный выпуск основной ветки разработки; добавление метапакета без версии">
<correction libdbi "Повторно закомментирован вызов _error_handler() для исправляющия проблемы с клиентами">
<correction libembperl-perl "Обработка страниц ошибок из Apache &gt;= 2.4.40">
<correction libexif "Исправления безопасности [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093]; исправления безопасности [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; исправление переполнения буфера [CVE-2020-0182] и переполнения целых беззнаковых чисел [CVE-2020-0198]">
<correction libvncserver "Исправление переполнения динамической памяти [CVE-2019-15690]">
<correction linux "Новый стабильный выпуск основной ветки разработки; обновление ABI до версии 4.9.0-13">
<correction linux-latest "Обновление ABI ядра до версии 4.9.0-13">
<correction mariadb-10.1 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Добавление поддержки для нового формата ссылок mega.nz">
<correction mod-gnutls "Прекращение использования устаревших наборов шифров в тестах; Исправление ошибок тестирования при использовании исправления Apache для CVE-2019-10092">
<correction mongo-tools "Повторная сборка с использованием свежей версии golang для добавления исправлений безопасности">
<correction neon27 "Трактовка связанных с OpenSSL ошибок тестирования как нефатальных">
<correction nfs-utils "Исправление потенциальной перезаписи файлов [CVE-2019-3689]; не устанавливать пользователя statd владельцем всех /var/lib/nfs">
<correction nginx "Исправление подделки запроса страницы с сообщением об ошибке [CVE-2019-20372]">
<correction node-url-parse "Корректировка путей и имён узлов до выполнения грамматического разбора [CVE-2018-3774]">
<correction nvidia-graphics-drivers "Новый стабильный выпуск основной ветки разработки; новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Исправление отсутствующей зависимости от libvtk6-qt-dev">
<correction perl "Исправление многочисленных проблем безопасности, связанных с регулярными выражениями [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Исправление межсайтового скриптинга [CVE-2020-8035]">
<correction php-horde-data "Исправление удалённого аутентифицированного выполнения кода [CVE-2020-8518]">
<correction php-horde-form "Исправление удалённого аутентифицированного выполнения кода [CVE-2020-8866]">
<correction php-horde-gollem "Исправление межсайтового скриптинга в выводе breadcrumb [CVE-2020-8034]">
<correction php-horde-trean "Исправление удалённого аутентифицированного выполнения кода [CVE-2020-8865]">
<correction phpmyadmin "Несколько исправлений безопасности [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "Новый стабильный выпуск основной ветки разработки">
<correction proftpd-dfsg "Исправление обработки пакетов SSH_MSG_IGNORE">
<correction python-icalendar "Исправление Python3-зависимостей">
<correction rails "Исправление возможного межсайтового скриптинга через вспомогательный код экранирования Javascript [CVE-2020-5267]">
<correction rake "Исправление введения команд [CVE-2020-8130]">
<correction roundcube "Исправление межсайтового скриптинга через HTML-сообщения, содержащие вредоносный компонент svg/namespace [CVE-2020-15562]">
<correction ruby-json "Исправление небезопасного создания объектов [CVE-2020-10663]">
<correction ruby2.3 "Исправление небезопасного создания объектов [CVE-2020-10663]">
<correction sendmail "Исправление обнаружения управляющего очередью процесса в режиме <q>split daemon</q>, <q>NOQUEUE: соединение от (null)</q>, удаление ошибки при использовании BTRFS">
<correction sogo-connector "Новая версия основной ветки разработки, совместимая с новыми версиями Thunderbird">
<correction ssvnc "Исправление записи за пределы выделенного буфера памяти [CVE-2018-20020], бесконечного цикла [CVE-2018-20021], неправильной инициализации [CVE-2018-20022], потенциального отказа в обслуживании [CVE-2018-20024]">
<correction storebackup "Исправление возможного повышения привилегий [CVE-2020-7040]">
<correction swt-gtk "Исправление отсутствующей зависимости от libwebkitgtk-1.0-0">
<correction tinyproxy "Создание PID-файла перед сбросом привилений до отличной от суперпользователя учётной записи [CVE-2017-11747]">
<correction tzdata "Новый стабильный выпуск основной ветки разработки">
<correction websockify "Исправление отсутствующей зависимости от python{3,}-pkg-resources">
<correction wpa "Исправление режима обхода защиты, не позволяющей преждевременно завершать PMF-соединение в режиме точки доступа [CVE-2019-16275]; исправление проблем со случайным назначением MAC-адреса для некоторых карт">
<correction xdg-utils "Очистка имени окна до его отправки по D-Bus; правильная обработка каталогов с именами, содержащими пробелы; создание каталога <q>applications</q> при необходимости">
<correction xml-security-c "Исправление длины вычисления в методе concat">
<correction xtrlock "Исправление блокировки (некоторых) устройств с мультисенсорными панелями при блокировке экрана [CVE-2016-10894]">
</table>


<h2>Обновления безопасности</h2>


<p>В данную редакцию внесены следующие обновления безопасности предыдущего
стабильного выпуска. Команда безопасности уже выпустила рекомендации для
каждого из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction certificatepatrol "Несовместим с новыми версиями Firefox ESR">
<correction colorediffs-extension "Несовместим с новыми версиями Thunderbird">
<correction dynalogin "Зависит от удаляемого пакета simpleid">
<correction enigmail "Несовместим с новыми версиями Thunderbird">
<correction firefox-esr "[armel] Более не поддерживается (требует nodejs)">
<correction firefox-esr "[mips mipsel mips64el] Более не поддерживается (требует новую версию rustc)">
<correction getlive "Сломан из-за изменений Hotmail">
<correction gplaycli "Сломан из-за изменений Google API">
<correction kerneloops "Служба основной ветки разработки более не доступна">
<correction libmicrodns "Проблемы безопасности">
<correction libperlspeak-perl "Проблемы безопасности; не сопровождается">
<correction mathematica-fonts "Использует недоступный для загрузки ресурс">
<correction pdns-recursor "Проблемы безопасности; не поддерживается">
<correction predictprotein "Зависит от удаляемого пакета profphd">
<correction profphd "Не может использоваться">
<correction quotecolors "Несовместим с новыми версиями Thunderbird">
<correction selenium-firefoxdriver "Несовместим с новыми версиями Firefox ESR">
<correction simpleid "Не работает с PHP7">
<correction simpleid-ldap "Зависит от удаляемого пакета simpleid">
<correction torbirdy "Несовместим с новыми версиями Thunderbird">
<correction weboob "Не сопровождается; удалён из более поздних выпусков">
<correction yahoo2mbox "Сломан на протяжении нескольких лет">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной
редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий предыдущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Предлагаемые обновления для предыдущего стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Информация о предыдущем стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian — объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
