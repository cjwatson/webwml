#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en git, un sistema de control
de versiones distribuido, rápido y escalable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>Se informó de que la opción --export-marks de git fast-import está
    expuesta también por medio de la orden feature export-marks=... en la entrada («in-stream»),
    lo que permite que se sobrescriban rutas arbitrarias.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>Se descubrió que los nombres de los submódulos no se validan de forma lo suficientemente
    estricta, lo que permite ataques muy dirigidos mediante ejecución de código remoto
    al realizar clonaciones recursivas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Joern Schneeweisz informó de una vulnerabilidad por la cual una clonación recursiva
    seguida de la actualización de un submódulo podía ejecutar código contenido en
    el repositorio sin que el usuario lo pida explícitamente. Ahora
    no se acepta que `.gitmodules` incluya entradas de la forma
    `submodule.&lt;name&gt;.update=!command`.</p></li>

</ul>

<p>Además, esta actualización aborda varias cuestiones de seguridad que solo
son un problema si git opera en un sistema de archivos NTFS (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> y <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>
<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 1:2.11.0-3+deb9u5.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:2.20.1-2+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de git.</p>

<p>Para información detallada sobre el estado de seguridad de git, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
