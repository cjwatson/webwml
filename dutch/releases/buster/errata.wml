#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ae94b634b4bdf58d58f44bc623b8cd6179f7fc13"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor informatie over
eventuele beveiligingsproblemen die in <q>buster</q> ontdekt werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
   deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 10.1, werd uitgebracht op
      <a href="$(HOME)/News/2019/20190907">7 september 2019</a>.</li>
  <li>De tweede tussenrelease, 10.2, werd uitgebracht op
      <a href="$(HOME)/News/2019/20191116">16 november 2019</a>.</li>
  <li>De derde tussenrelease, 10.3, werd uitgebracht op
      <a href="$(HOME)/News/2020/20200208">8 februari 2020</a>.</li>
 <li>The vierde tussenrelease, 10.4, werd uitgebracht op
      <a href="$(HOME)/News/2020/20200509">9 mei 2020</a>.</li>
</ul>



<ifeq <current_release_buster> 10.0 "

<p>Er zijn nog geen tussenreleases voor Debian 10.</p>" "

<p>Zie de
<a href="http://ftp.nl.debian.org/debian/dists/buster/ChangeLog">ChangeLog</a>
voor details over wijzigingen tussen 10 en <current_release_buster/>.</p>"/>

<p>Verbeteringen voor de uitgebracht stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> van elke Debian archief-spiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde updates voor een 10 tussenrelease
  deb http://ftp.nl.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Raadpleeg voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
