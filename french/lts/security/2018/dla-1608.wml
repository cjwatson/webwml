#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Des vulnérabilités ont été découvertes dans php5, un langage de script
embarqué dans du HTML et côté serveur. Remarquez que cette mise à jour inclut
une modification du comportement par défaut pour les connexions IMAP. Voir
ci-dessous pour les détails.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19518">CVE-2018-19518</a>

<p>Une vulnérabilité d’injection d’argument dans imap_open() peut permettre à un
attaquant distant d’exécuter des commandes d’OS arbitraires sur le serveur
IMAP.</p>

<p>Le correctif pour la vulnérabilité
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19518">CVE-2018-19518</a>
incluait cette note supplémentaire des développeurs amont :</p>

<p>À partir de 5.6.38, les connexions rsh/ssh sont désactivées par défaut.
Utiliser imap.enable_insecure_rsh pour pouvoir les activer. Notez que la
bibliothèque IMAP ne filtre pas les noms de boîte aux lettres avant de les
passer à la commande rsh/ssh, par conséquent passer des données non fiables à
cette fonction avec rsh/ssh activé est non sûr.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19935">CVE-2018-19935</a>

<p>Un déréférencement de pointeur NULL conduit à un plantage d'application et
à un déni de service à l'aide d'une chaîne vide dans l’argument du message de la
fonction imap_mail de ext/imap/php_imap.c.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 5.6.39+dfsg-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1608.data"
# $Id: $
