#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité dans dropbear, un serveur et client SSH2 léger, rendant
possible de deviner des noms d’utilisateur valables, a été découverte :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15599">CVE-2018-15599</a>

<p>la fonction recv_msg_userauth_request dans svr-auth.c est prédisposée à une
vulnérabilité d’énumération d’utilisateurs, similaire à
<a href="https://security-tracker.debian.org/tracker/CVE-2018-15473">CVE-2018-15473</a>
dans OpenSSH.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2014.65-1+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets dropbear.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1476.data"
# $Id: $
