#use wml::debian::translation-check translation="d7fbd2143d5753c8d99eb6ab0e3aa4fbaf445b22" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème d’injection de code a été découvert dans PyXDG, une bibliothèque
utilisée pour trouver les répertoires configuration/cache/etc. de
<a href="https://www.freedesktop.org/">freedesktop.org</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12761">CVE-2019-12761</a>

<p>Un problème d’injection de code a été découvert dans PyXDG avant 0.26,
réalisée à l’aide de code Python contrefait dans un élément Category d’un
document XML Menu dans un fichier .menu. XDG_CONFIG_DIRS doit être configuré
pour déclencher une analyse xdg.Menu.parse à l’intérieur du répertoire contenant
ce fichier. Cela est dû à un manque de vérification dans xdg/Menu.py avant un
appel eval.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.25-4+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets pyxdg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1819.data"
# $Id: $
