#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans pdns, un serveur DNS
faisant autorité.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2120">CVE-2016-2120</a>

<p>Mathieu Lafon a découvert que pdns ne valide pas correctement les entrées
par zones. Un utilisateur autorisé peut exploiter ce défaut pour planter un
serveur par l'insertion d'une entrée contrefaite pour l'occasion dans une zone
sous son contrôle puis par l'envoi ultérieur d'une requête DNS pour cette entrée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7068">CVE-2016-7068</a>

<p>Florian Heinz et Martin Kluge ont signalé que pdns analyse toutes les entrées
présentes dans une requête indépendamment du fait qu'elles soient nécessaires ou
même justifiées, permettant à un attaquant authentifié distant de provoquer une
charge anormale du CPU sur le serveur pdns, aboutissant à un déni de service
partiel si le système devient surchargé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7072">CVE-2016-7072</a>

<p>Mongo a découvert que le serveur web de pdns est exposé à une vulnérabilité
de déni de service. Un attaquant non authentifié distant peut provoquer un déni
de service en ouvrant un grand nombre de connexions TCP au serveur web.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7073">CVE-2016-7073</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-7074">CVE-2016-7074</a>

<p>Mongo a découvert que pdns ne valide pas assez les signatures TSIG, permettant
à un attaquant en position « d'homme du milieu » de modifier le contenu d'un AXFR.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.1-4.1+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pdns.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-798.data"
# $Id: $
