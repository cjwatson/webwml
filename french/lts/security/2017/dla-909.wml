#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7960">CVE-2017-7960</a>

<p>Une vulnérabilité de lecture hors limites de tampon de tas pourrait être
déclenchée à distance à l'aide d'un fichier CSS contrefait pour provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7961">CVE-2017-7961</a>

<p>Un problème de comportement indéfini <q>en dehors de l'intervalle des valeurs représentables
par le type long</q> a été découvert dans libcroco. Cela pourrait permettre
à des attaquants distants de provoquer un déni de service (plantage
d'application) ou éventuellement d'avoir un impact non précisé à l'aide d'un
fichier CSS contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.6.6-2+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libcroco.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-909.data"
# $Id: $
